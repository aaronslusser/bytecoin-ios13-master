//
//  Price.swift
//  ByteCoin
//
//  Created by  Amber  on 10/25/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//

import Foundation


struct Price: Decodable {
    let time: String
    let asset_id_base: String
    let asset_id_quote: String
    let rate: Double
}
